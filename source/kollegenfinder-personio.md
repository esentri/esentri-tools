---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/kollegenfinder-personio/Folie1.png)

(pause: 2)

Hallo und herzlich Willkommen zum Kollegenfinder-Onboarding. Schön, dass du da bist!
Wenn du in deinem Geschäftskreis die Kollegenfinder-Rolle inne hast, dann bist du hier genau richtig.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie2.png)

Wir freuen uns, dass du den Recruiting-Prozess unterstützt und mit uns gemeinsam darauf hin arbeitest, die richtigen Leute für esenntrie zu gewinnen.
Das Recruiting ist ein entscheidender Erfolgsfaktor für uns. In dem wir die richtigen Menschen für uns begeistern, stellen wir die Weichen für mehr Wachstum und die Erreichung unserer gemeinsamen Ziele.

(pause: 1)

Aus Gründen der Einfachheit verzichten wir im gesprochenen Text auf eine gendergerechte Formulierung.
Angesprochen sind selbstverständlich alle Geschlechter.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie3.png)

Heute möchten wir mit dir über die Rollen und Verantwortlichkeiten im Recruiting-Prozess, die Regeltermine, die zentralen Tools und deine Aufgaben sprechen. Außerdem erhältst du einen ersten Überblick über Personio.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie4.png)

Im gesamten Recruiting-Prozess gibt es verschiedene Rollen und Verantwortlichkeiten, die wir dir gerne vorstellen möchten.

(pause: 1)

Der Future Relations Planer erstellt die jährliche Personalbedarfsplanung in Abstimmung mit den Kollegenfindern und Repräsentanten der verschiedenen Kreise.
Der Planer sorgt außerdem für die Anpassung der Personalbedarfsplanung, falls sich Änderungen ergeben. Aktuell füllen Moritz und Romana diese Rolle gemeinsam aus.

(pause: 1)

Der Reisebegleiter steuert den Recruiting-Prozess von Anfang bis Ende und sorgt dafür, dass sich die Kandidaten im Prozess gut aufgehoben fühlen und eine exzellente Candidate Journey erleben.
Moritz und Vanessa übernehmen diese Rolle gemeinsam.

(pause: 1)

Nun zum Stellenexperten. Dieser ist euer Ansprechpartner, wenn es um die Aufnahme der Stellen aus den Kreisen, die Erstellung von Stellenanzeigen sowie deren Veröffentlichung geht.
Aktuell ist Vanessa eure Ansprechpartnerin.

(pause: 1)

Eine weitere wichtige Rolle übernimmt der Netzwerker. Er sorgt für die Organisation und Durchführung von Messen und Hochschulkooperationen sowie die Netzwerkpflege und die Durchführung von Recruiting-Events. Darüber hinaus geht der Netzwerker aktiv auf die Suche nach Talenten. Die Rolle des Netzwerkers übernehmen Moritz und Vanessa gemeinsam.

(pause: 1)

Nun zu dir als Kollegenfinder! Du unterstützt das Future Relations Team bei der Evaluation von Bewerbungen und führst gemeinsam mit Future Relations Erstgespräche durch.
Außerdem berätst du deinen Repräsentanten.

(pause: 1)

Der Repräsentant eines Kreises evaluiert ebenfalls Bewerbungen und führt gemeinsam mit Future Relations die Zweitgespräche durch. Am Ende trifft der Repräsentant die Einstellungsentscheidung.


(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie5.png)

Bevor wir uns nun die zentralen Tools gemeinsam anschauen, lasst uns schnell auf die wichtigsten zu beachtenden Regeltermine schauen.

(pause: 1)

Die Praktikergruppe Kollegenfinder trifft sich jeden Monat zum Austausch in der Gruppe. Eine Termineinladung erhältst du von Future Relations. Neben dem jährlichen Kollegenfinder-Kick-Off, finden außerdem halbjährliche Retrospektiven statt.

(pause: 1)

Das Future Relations Team hat 3 Weeklys pro Woche und steht in ständigem Austausch miteinander. Dadurch wird sichergestellt, dass wir zeitnah im Prozess reagieren können und lange Wartezeiten für Bewerber vermeiden.
Für den Austausch mit den Geschäftskreisen steht das Team jederzeit nach Rücksprache zur Verfügung. Kommt gerne auf das Future Relations Team zu!

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie6.png)

Jetzt aber zu den zentralen Tools, die wir für den gesamten Recruiting-Prozess benötigen.
Unser wohl wichtigstes Tool nennt sich Personio. Personio ist unser Personalmanagementsystem, womit wir Bewerber verwalten, Stellenanzeigen schalten und Mails an Kandidaten versenden können.
Einen kurzen Überblick gibt es am Ende des Videos.

(pause: 1)

Die Kommunikation mit dem Future Relations Team wird über den Google Chat abgebildet. Dort findest du auch eine gemeinsame Gruppe für deinen Geschäftskreis und Future Relations.
Der Google Chat ist somit das schnellste Mittel der Wahl.

(pause: 1)

In Confluence findest Du sämtliche Infos zur Praktikergruppe Kollegenfinder. Du siehst zum Beispiel welche Mitglieder die Gruppe beinhaltet und erhältst nochmal alle Infos zu den verschiedenen Rollen.
Die Kollegenfinder-Gruppe in Yammer wird für die ergänzende, formalere Kommunikation genutzt.


(pause: 1)

Und zu guter Letzt, nutzen wir ein Miro-Board für unsere gemeinsamen Retrospektiven.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie7.png)

Du fragst dich jetzt bestimmt, was sind eigentliche meine Aufgaben als Kollegenfinder und welche Erwartungen werden an mich gestellt?

Wenn du folgende Punkte beachtest, kannst du eigentlich nichts falsch machen:

(pause: 1)

Gib deine Einschätzung zu Bewerbungen ab. Personio informiert dich über neue Bewerbungen per E-Mail. Idealerweise schreibst du deine Einschätzung innerhalb von 24h in das Kommentarfeld des Bewerbers auf Personio. Kommuniziere dabei klar und deutlich, was die nächsten Schritte im Prozess sind.

(pause: 1)

Vorbereitung ist Alles! Schau dir die Unterlagen des Kandidaten vor dem Vorstellungsgespräch an und überlege dir fachliche Fragen, die du stellen kannst.
Im Gespräch solltest du außerdem über esenntrie im Allgemeinen, deinen Geschäftskreis und die offene Stelle sprechen können.
Denke immer daran, dass wir den Kandidaten für esenntrie begeistern wollen! Es ist unsere Aufgabe, esenntrie bestmöglich zu repräsentieren.

(pause: 1)

Am Ende ist dein Feedback und der Austausch mit Future Relations wichtig. Idealerweise gibst du dein Feedback innerhalb von 24h als Kommentar in Personio ab.
Bitte kommuniziere auch hier wieder klar und deutlich die nächsten Schritte.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie8.png)

Last but not least, hier noch ein kleiner Überblick über Personio, damit du loslegen kannst!
Falls du tiefergehende Fragen zu Personio hast, wende dich gerne an dein Future Relations Team.

(pause: 1)

Öffne zunächst okta und wähle dort unter "Meine Apps" Personio aus. Melde dich an.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie9.png)

Links in der Menüleiste findest du das Recruiting-Modul. Klicke es an.

(pause: 1)

Unter dem Reiter „Stellen“ findest Du alle offene Positionen aus Deinem Geschäftskreis. Du siehst die jeweilige Stellenbeschreibung und auf welchen Kanälen diese veröffentlicht wurde.  
Darüber hinaus hast Du einen Überblick über alle bisherigen Bewerber zu der jeweiligen Stelle.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie10.png)

Unter dem Reiter „Bewerbungen“ findest Du alle Bewerber für die offenen Stellen in deinem Geschäftskreis. Du kannst verschiedene Filter anwenden, um Dir einen besseren Überblick zu verschaffen. Neue Nachrichten oder Kommentare, werden in der Übersicht mit einem blauen Kreis angezeigt.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie11.png)

Wenn Du einen Bewerber aus der Liste anklickst, kommst Du auf die Bewerber-Übersicht zur ausgewählten Person. Hier findest Du alle Personendaten, alle relevanten Unterlagen, den Nachrichtenverlauf sowie eine Kommentarfunktion, in der Du deine Einschätzung, beziehungsweise dein Feedback, abgeben kannst.
Future Relations pflegt alle Personendaten und den aktuellen Status im Prozess ein.
Ebenso werden Nachrichten an Bewerber von Future Relations versendet.


(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie12.png)

Das Kommentarfeld nutzt Du, um Deine Einschätzung, beziehungsweise dein Feedback, abzugeben. Idealerweise folgt Deine Einschätzung innerhalb von 24 Stunden.
Mit dem grünen Positiv-Button, zeigst Du zusätzlich zum Kommentar an, dass Du im Prozess weitermachen möchtest.
Mit dem roten Negativ-Button zeigst Du an, dass Du im Prozess nicht weitermachen möchtest.
Den grauen Neutral-Button verwendest Du, wenn Du keine klare Präferenz in eine Richtung hast und Dir noch unschlüssig bist, oder Du einen sonstigen Kommentar abgeben möchtest.
Wichtig ist, dass aus dem Kommentar der nächste logische Schritt ersichtlich ist.
Zum Beispiel: Future Relations soll die Reisebereitschaft vorab klären.

(pause: 2)
---
![contain](slides/kollegenfinder-personio/Folie13.png)

Nun sind wir am Ende angelangt. Vielen Dank fürs Zuhören! Wir hoffen, dass dir das Kollegenfinder Onboarding gefallen hat und du nun ein klares Bild über deine neue Rolle hast.
Falls du noch offene Fragen hast, wende dich gerne an dein Future Relations Team, also an Moritz und Vanessa.
Bis bald!
---
