---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/e-sign/Folie1.png)

(pause: 1)

Herzlich willkommen zur Einführung in HelloSign - unserem Tool für elektronische Signaturen.

(pause: 2)
---
![contain](slides/e-sign/Folie2.png)

In diesem Video möchten wir dir eine Einführung in die Nutzung von HelloSign geben.

(pause: 1)

Wir beginnen mit einigen generellen Infos zur elektronischen Signatur und zur Nutzung des Tools. Danach zeigen wir dir beispielhaft die Bedienoberfläche des Tools und erklären, wie du deine Dokumente erstellen, unterschreiben und versenden kannst. HelloSign verfügt auch über Plug-ins, die deine Arbeitsprozesse erleichtern können.

(pause: 1)

Am Ende gehen wir noch kurz auf den Sonderfall, die qualifizierte elektronische Signatur ein.

(pause: 2)
---
![contain](slides/e-sign/Folie3.png)

Unter einer elektronischen Signatur versteht man mit elektronischen Informationen verknüpfte Daten, mit denen der Unterzeichner eines Dokuments identifiziert und die Integrität der signierten Informationen geprüft werden kann.

(pause: 1)

Die elektronische Unterschrift kann genauso wirksam wie eine handschriftliche Unterschrift sein. Die europäische e IDAS Verordnung setzt Rahmenlinien und sorgt für standardisierte Prozesse.

(pause: 1)

Man unterscheidet zwischen drei Arten der elektronischen Signatur.

(pause: 1)

Die einfache elektronische Signatur (E E S) ermöglicht keine Identifizierung der anderen Person. Es handelt sich um Daten, die in elektronischer Form anderen elektronischen Daten beigefügt werden. Sie ist einfach zu handhaben, jedoch schlechter beweisbar. Die einfache elektronische Signatur reicht üblicherweise für interne Dokumente, und Dokumente ohne gesetzliche Formvorschrift aus.

(pause: 1)

Die fortgeschrittene elektronische Signatur (F E S) ist die für die meisten Dokumente verwendete Unterschrift. Die erfassten Daten des Unterzeichners ermöglichen im Nachhinein eine Identitätsprüfung des Unterzeichners, beispielsweise durch Unterzeichnungsdatum und -uhrzeit sowie EI P Adresse. Alle mit HelloSign unterschriebenen Dokumente erfüllen mindestens die Voraussetzungen einer fortgeschrittenen elektronischen Signatur.

(pause: 1)

Die qualifizierte elektronische Signatur (Q E S) entspricht der handschriftlichen Unterschrift. Dafür muss die Identität der Person vor der Unterschrift geprüft werden, beispielsweise durch Video-Chats mit zertifizierten Trust-Centern. Sie wird nur sehr selten benötigt, zum Beispiel bei Bankgeschäften oder Ausschreibungen. Der Unterzeichner muss seine Identität nachweisen, bevor eine qualifizierte Unterschrift geleistet werden kann.

(pause: 2)
---
![contain](slides/e-sign/Folie4.png)

Die Nutzung von HelloSign, unserem Tool für die elektronische Signatur, ist für jeden freigeschaltet. Jeder, der im Arbeitskontext selbst Signaturen leisten muss oder Signaturen anfragen muss kann HelloSign verwenden. Der Zugriff erfolgt hierbei über Okta.

(pause: 1)

Bestimmte Mitarbeiter und Mitarbeiterinnen sind vom Vorstand dazu bevollmächtigt, Unterschriften in Vollmacht oder im Auftrag des Vorstands zu leisten. Die Unterschriftenregelung kann in Confluence eingesehen werden.

(pause: 1)

Der Standardfall ist die fortgeschrittene elektronische Signatur. Alle über HelloSign geleisteten oder angeforderten Signaturen entsprechen automatisch den Voraussetzungen dafür. In Ausnahmefällen kann auch eine qualifizierte elektronische Signatur nötig sein, dies wird dann jedoch explizit gefordert.

(pause: 2)
---
![contain](slides/e-sign/Folie5.png)

Nun folgen einige Infos zur Nutzung von HelloSign.

(pause: 1)

Wir starten im ersten Schritt mit den Einstellungen. Diese findest Du rechts oben, versteckt hinter Deinen Initialen. Hier kannst Du im ersten Schritt Deine eigene Unterschrift hinterlegen.

(pause: 1)

Unter Einstellungen müssten alle Informationen zur esenntrie, wie Logo, Slogan und Signatur bereits hinterlegt sein. Bitte prüfe dies und ergänze, falls dies nicht der Fall wäre. Dies ist wichtig für die ausgehende Mail, die die zu unterzeichnende Person am Ende jedes Prozesses erhält.

(pause: 1)

Im Linken Menürand findest Du nun alle weiteren wichtigen Bausteine von Hellosign. Hierunter auch potenzielle Vorlagen, bereits früher signierte Dokumente und auch potenzielle Integrationen auf die wir später noch mal eingehen werden.

(pause: 2)

---
![contain](slides/e-sign/Folie6.png)


Um Dateien zu versenden oder zu unterschreiben kann das jeweilige Dokument beispielsweise im Word oder pdf Format in HelloSign hochgeladen werden. Durch die Nutzung von Plug-ins in Google Mail oder Docs kann das Hochladen übersprungen werden. Für wiederkehrende Dokumente können Vorlagen erstellt werden. Es können auch mehrere Dokumente auf ein mal hochgeladen werden.

(pause: 2)

---
![contain](slides/e-sign/Folie7.png)

Im nächsten Schritt können dann die Unterzeichner hinzugefügt werden.

(pause: 1)

Falls du das Dokument nur selbst unterschreiben musst, kannst du oben rechts, “Ich bin der einzige Unterzeichner” auswählen.

(pause: 1)

Weitere Unterzeichner können durch Eingabe deren Namen und E-Mail Adressen hinzugefügt werden.

(pause: 1)

Zusätzliche Sicherheitsvorkehrungen können getroffen werden, sodass der Unterzeichner beispielsweise einen Zugriffscode für die Öffnung des Dokuments angeben muss. Dafür kann auf die drei Punkte neben der E-Mail Adresse des Unterzeichners geklickt werden und “Unterzeichnerauthentifizierung hinzufügen” ausgewählt werden.

(pause: 1)


Falls sich Ansprechpartner und Unterzeichner unterscheiden, kann der Haken bei “Unterzeichner-Neuzuweisung aktivieren” gesetzt werden.

Bei mehreren Unterzeichnern kann die Reihenfolge festgelegt werden, oder alle erhalten das Dokument gleichzeitig.

Um die Reihenfolge zu verändern, schiebe die Namen durch die 3 Punkte auf der Linken Seite einfach in die entsprechende Reihenfolge von oben nach unten.

(pause: 2)
---
![contain](slides/e-sign/Folie8.png)

Unterschriftenfelder können per Drag & Drop in das Dokument gezogen und durch Anklicken zugewiesen werden. Wem die Felder zugewiesen werden, kannst Du im oberen Linken Feld auswählen. Anhand der verschiedenen Farben kannst du die Zuweisung nachvollziehen. Achte stets auf die genaue Zuordnung der Felder.

(pause: 1)

Neben den Unterschriftenfeldern können Namensfelder, Datumsfelder, Titel usw. hinzugefügt werden, auch weitere Felder lassen sich definieren.
Die Felder können nach belieben größer und kleiner gezogen werden.

(pause: 1)

Bitte achte darauf, dass insbesondere die Namensfelder groß genug sind, da ansonsten evtl. nicht der ganze Name sichtbar sein wird.


(pause: 2)
---
![contain](slides/e-sign/Folie9.png)

So, kann ein signiertes Dokument aussehen.

Die Signatur kann getippt oder handschriftlich eingefügt werden. Außerdem können Unterschriften gespeichert werden.

Unterschriften in Vollmacht oder im Auftrag können im Namensfeld vermerkt werden.

(pause: 1)

Das finale, von allen Unterzeichnern signierte Dokument wird automatisch allen Unterzeichnern zugesendet.

(pause: 2)
---
![contain](slides/e-sign/Folie10.png)

An das finale Dokument wird ein Prüfprotokoll angehängt. Hier ist der Arbeitsprozess einsehbar. Man kann erkennen, wer das Dokument wann geöffnet, unterschrieben oder weitergeleitet hat. Auch E-Mail Adressen und Ei P-Adressen sind hier vermerkt.

(pause: 1)

Der Sender kann selbst auswählen, ob das Prüfprotokoll an das gezeichnete Dokument angehängt oder vom Dokument separiert werden soll. Dies lässt sich unter Einstellungen entsprechend anpassen.

(pause: 2)
---
![contain](slides/e-sign/Folie11.png)

Für effizientere Arbeitsprozesse können Plug-ins genutzt werden. Dies sorgt für nahtlose und reibungslose Arbeitsabläufe zwischen all unseren Systemen.

(pause: 1)

Neben einer offenen API-Schnittstelle gibt es bereits verschiedene Integrationen zu Microsoft Word, Google Workspace, Salesforce, Habspot und vielen weiteren.

Diese können direkt über die Homepage von Hellosign oder über die jeweiligen Stores heruntergeladen und als Add-On hinzugefügt werden.

(pause: 1)

Bei Unklarheiten kann man sich hierzu jederzeit an unseren Dienstleistungskreis IT wenden.


(pause: 1)

Im weiteren gehen wir auf die ein oder andere Integration etwas näher ein.


(pause: 2)
---
![contain](slides/e-sign/Folie12.png)

Das erste Beispiel ist das Google Mail Plug In.

(pause: 1)

Nach der Installation erkennt das Google Plug-in automatisch, wenn eine Mail mit Anhang empfangen wurde.

Zu unterzeichnende Dokumente können so direkt ausgewählt und in HelloSign hochgeladen werden.

(pause: 2)
---
![contain](slides/e-sign/Folie13.png)

Das zweite Beispiel ist die Integration in Google Docs oder in Microsoft Word.

(pause: 1)

Dies ermöglicht eine direkte Weiterleitung in Hellosign und eine direkte Erzeugung einer signierten PDF ohne diese separat abzusichern und in einem weiteren Schritt zu Hellosign hinzuzufügen.

In beiden Fällen kann man unterscheiden zwischen "selbst signieren" oder "Signaturen anfragen".
---
![contain](slides/e-sign/Folie14.png)

Nun gehen wir auf den Sonderfall, die qualifizierte elektronische Signatur ein. Diese wird verkürzt Q E S genannt.

Eine Q E S hat die selbe rechtliche Gewichtung wie eine persönliche Identifizierung und wird zum Beispiel bei größeren Ausschreibungen häufiger verwendet.

(pause: 1)

Das Vorgehen ist dabei recht leicht. Man bereitet die Signatur wie gewohnt vor.

(pause: 1)

Beim Hinzufügen der signierenden Nutzer wird noch „Qualifizierte elektronische Signatur aktivieren“ ausgewählt. Dadurch werden die Q E S Anforderungen zur Anfrage hinzugefügt und Unterzeichner müssen ihre Identität mit unserem Partner EI D now verifizieren.

Ohne die Signatur Seite von HelloSign zu verlassen, wird ein neues Fenster geöffnet und Unterzeichner werden zur Identitätsprüfung an EI D now weitergeleitet. Hier führen vertrauenswürdige EI D now Agenten einen Videoanruf mit Deinen Unterzeichnern durch, um ihre Identität zu überprüfen.

Sobald die Unterzeichner erfolgreich verifiziert sind, bestätigen sie ihre Signatur per S M S. Als Nächstes generiert ein Trusted Service Provider ein digitales Zertifikat und wendet es auf die Signatur der Unterzeichner an. Alle Absender erhalten am Ende ein signiertes Exemplar der Vereinbarung.

(pause: 2)
---
![contain](slides/e-sign/Folie15.png)

Vielen Dank fürs Zuhören! Wir hoffen dir hat dieses Video weitergeholfen. Bei Fragen kannst du dich gerne an die IT wenden oder ein IT Service Ticket erstellen.
---
