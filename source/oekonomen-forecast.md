---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/oekonomen-forecast/Folie1.png)

(pause: 2)

Hallo und herzlich Willkommen zu einer Einführung in das Erstellen von monatlichen Finance Forecasts. Dies ist in erster Linie für die Ökonomen-Rolle relevant.

(pause: 2)
---
![contain](slides/oekonomen-forecast/Folie2.png)


Zunächst unterscheiden wir in drei Kategorien, in die das Budget eines Kreises fallen kann. Eine Version eines Budgets kann immer nur in eine der Kategorien fallen.

(pause: 1)

Die Ist-Zahlen eines Kreises werden ausschließlich von Finn Opps gepflegt. Sie werden in der Mitte eines Monats für den vorherigen, abgeschlossenen Monat gepflegt. Diese Kategorie eignet sich um zu prüfen, wie sich die Finanzzahlen eines Kreises im Vergleich zum Plan entwickeln.

(pause: 1)

Die Plan-Zahlen entstehen bevor ein neues Geschäftsjahr beginnt. In der Regel beginnt der Prozess zur Finanzplanung bereits im Oktober eines Jahres für das Folgejahr. Diese Zahlen entstehen in enger Zusammenarbeit zwischen der Repräsentanten-, der Ökonomen-Rolle, Finn Opps und der Geschäftsführung. Aufgrund der Langfristigkeit sowie der Komplexität ist es üblich dass mehrere Versionen erstellt werden. Solange bis eine finale Planung über das kommende Geschäftsjahr vorliegt.

(pause: 1)

Eine einheitliche Benamung hilft Finn Opps den Überblick zu behalten.

(pause: 1)

Nun zur der Kategorie die in diesem Video im Mittelpunkt steht: der Forecast. Der Forecast wird monatlich in einer überarbeiteten Version erstellt. Hier fließen alle Neuigkeiten und Änderungen ein die die Finanzzahlen eines Kreises beeinflussen. Gibt es keine Änderungen, so muss auch keine neue Version eingereicht werden. Auch hier ist die einheitliche Benamung wichtig.  

(pause: 2)
---
![contain](slides/oekonomen-forecast/Folie3.png)

Bevor wir in die Details der Erstellung eintauchen, lasst uns schnell auf die wichtigsten zu beachtenden Dinge schauen.

(pause: 1)

Änderungen die den aktuellen Monat oder auch die Folgemonate betreffen, sind jeweils bis zum dreizehnten eines Monats einzureichen. Pro Monat soll maximal eine neue Version eingereicht werden.

(pause: 1)

Vergangenes ist vergangen, soll heißen, dass die abgeschlossenen Monate nicht mehr verändert werden. Auch wenn dies die Gesamtsumme einer Planung beeinflusst. Denn diese Prämisse macht den Forecast zu dem was er ist: eine Mischung aus den Ist-Zahlen der Vergangenheit und den Plan-Zahlen für die Zukunft mit aktuellen Erkenntnissen.

(pause: 1)

Änderungen sollen für Finn Opps deutlich gemacht werden, damit sie nachvollzogen werden können. So sollen also Positionen die komplett entfallen, beispielsweise weil eine Schulung doch nicht stattfinden soll, nicht komplett gelöscht, sondern genullt werden.

(pause: 1)

Darauf zahlt auch die Änderungsnachricht ein. Beim Einreichen einer Version ist die Änderungsnachricht so auszufüllen, dass für jede Änderung eine Information vorliegt was sich und aus welchen Gründen sich etwas verändert hat.  Mögliche Änderungsnachrichten sind:

Projekt von Robert Rakete beginnt statt im März erst im April.
Urlaub von Frank Frohsinn reduziert seinen Umsatz im September um 50 Prozent.
Michael Miami wird für Oktober und November aus M H P Projekt ausgeplant.
Tagessatz von Barbara Billing erhöht sich um 300 Euro ab Mai.
Oder:
Weiterbildung von Katrin Karriere im Dezember fällt aus.

(pause: 2)
---
![contain](slides/oekonomen-forecast/Folie4.png)

Nun die einzelnen Schritte für die Erstellung eines neuen Forecasts mit Lithosphere. Zunächst wird die aktuellste Forecast-Version geklont. Für den ersten Forecast eines Jahres ist die Plan-Version zu klonen.


(pause: 1)
---
![contain](slides/oekonomen-forecast/Folie5.png)

Mit Klick auf den Stift lässt sich die Version entsprechend umbenennen.


(pause: 1)
---
![contain](slides/oekonomen-forecast/Folie6.png)

Die Korrekte Benennung ist: F C, der aktuelle Monat, das aktuelle Jahr, sowie die Versionsnummer dieses einzelnen Forecasts.

(pause: 1)
---
![contain](slides/oekonomen-forecast/Folie7.png)

Außerdem ist das Planungsjahr zu beachten.


(pause: 2)
---
![contain](slides/oekonomen-forecast/Folie8.png)

Nun nehmen wir erste Änderungen vor. Der Umsatzplaner ist sehr hilfreich, um Änderungen betreffend der Projekteinsätze zu planen. Im Umsatzplaner eingetragene Umsätze werden direkt in die Planpositionen übertragen.

(pause: 1)

Im Umsatzplaner gibt man pro Eintrag die betreffende Person ein. Für die Planung von Umsätzen für unbekannte Neueinstellungen kann die Repräsentanten-Rolle eingetragen werden.

(pause: 1)

30 Urlaubstage sind bei esenntrie üblich. Bei Teilzeit ist dies dem Vertrag entsprechend anzupassen. Für den Kickoff, die Düsentrieb Camps und weitere Veranstaltungen veranschlagen wir üblicherweise 10 Tage pro Jahr.

(pause: 1)

Bei vollausgelasteten Personen kalkulieren wir aufgrund von internen Arbeiten oder auch möglichen Ausfällen mit maximal 90 Prozent. Wenn ein Projekt unterbrochen wird, beispielsweise aufgrund von Elternzeit, ist es hilfreich eine Zeile anzulegen, die bis zur Elternzeit plant, und eine weitere für die Zeit danach.

(pause: 1)

Die Tagessätze bei Kunden können bei der Projektleitung in Erfahrung gebracht werden. In Zeile 1 sieht man den Tagessatz von 1000 Franken gegenüber dem Kunden in der Schweiz, sowie die interne Verrechnung von 780 Euro gegenüber der Schweizer Gesellschaft und esenntrie Deutschland. In diesem Video werden wir die Berechnung des Beispiels noch ausführlich besprechen.

(pause: 1)

Im Laufe eines Jahres ist es sinnvoll mit mehreren Zeilen pro Projekt zu arbeiten, da Änderungen in einer Zeile auch immer Auswirkungen auf die Vergangenheit haben. Doch vergangene Monate sollen nicht mehr bearbeitet werden. Hilfreich kann es deshalb sein, in den Planpositionen eine Umsatzzeile zu duplizieren und die Zellen des aktuellen Monats sowie die der Zukunft mit Negativbeträgen zu negieren. Ist das passiert, kann mit dem Umsatzplaner das Projekt mit den neuen Bedienungen ab Zeitpunkt X aufgeplant werden.

(pause: 1)

In den Planpositionen können einzelne Zellen bearbeitet werden, die der Umsatzplaner dort hin übertragen hat. Jedoch ist es wichtig zu wissen dass jede Änderung im Umsatzplaner die Änderungen in den Planpositionen wieder überschreibt. Deshalb ist es einerseits sinnvoll so viel wie möglich mit dem Umsatzplaner zu arbeiten. Und andererseits bei Änderungen immer eine neue Zeile anzulegen, um Vergangenes unberührt zu lassen.

(pause: 2)
---
![contain](slides/oekonomen-forecast/Folie9.png)

Schauen wir nochmal auf das Beispiel der Verrechnung zwischen den Gesellschaften Schweiz und Deutschland.

(pause: 1)

Der Tagessatz liegt bei 1000 Franken. Dies erwirtschaftet ein in Deutschland angestellter esenntrie für die Gesellschaft Schweiz. Es muss mit der Gesellschaft der Anstellung, also Deutschland verrechnet werden.

Hierfür werden die Franken mit einem Kurs von null Komma 9 2 umgerechnet. Bei 1000 Franken also 920 Euro.

(pause: 1)

Für den Verrechnungssatz zwischen Gesellschaften werden 85 % angewandt. Bei 920 Euro sind dies 782 Euro. Auf den nächsten Zehner gerundet also 780 Euro.

(pause: 1)

Das ist der Standardfall. Natürlich können auch in der Schweiz angestellte, bei Kunden in Deutschland arbeiten. Das Verfahren ist dasselbe.

(pause: 2)
---
![contain](slides/oekonomen-forecast/Folie10.png)

Hier siehst Du alle relevanten Umrechnungskurse zwischen Währungen.

(pause: 4)
---
![contain](slides/oekonomen-forecast/Folie11.png)

Abschließend noch einen Blick auf die Entwicklung der Zahlen im Laufe eines Jahres. Der Anteil der Ist-Zahlen wird natürlich immer größer und damit der Zeitraum für den ein Forecast aktualisiert wird kleiner. Daraus ergibt sich ein immer konkreteres Bild über das laufende Geschäftsjahr, mit dem der Kreis, Finn Opps, die Geschäftsführung und der Aufsichtsrat arbeiten können.

(pause: 1)

Die Mischung aus Ist- und Forecast Zahlen werden dann immer mit den initialen, unveränderlichen Plan Zahlen vom Beginn des Jahres verglichen.

(pause: 2)
---
![contain](slides/oekonomen-forecast/Folie1.png)

Ich hoffe ich konnte Dir einen besseren Überblick über das Erstellen von Forecasts geben. Offene Punkte können jederzeit mit der Repräsentanten-Rolle sowie Finn Opps geklärt werden. Bis bald!

(pause: 2)
---
