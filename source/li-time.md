---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/li-time/Folie1.png)

(pause: 2)

Hallo und herzlich Willkommen zu einer Einführung in den richtigen Umgang mit L I time und das Erfassen von geleisteten Stunden.

(pause: 2)
---
![contain](slides/li-time/Folie2.png)


Wir verhelfen unseren Kunden zu Vorsprung gegenüber deren Wettbewerb. Wir machen dies in unterschiedlichen Bereichen: IT Excellence, Digitale Kultur, data and ey Ei sowie innovation and business.  

(pause: 1)

Auch wenn ein Stück Software häufig ein Ergebnis unserer Arbeit ist, hängen wir daran kein Preisschild. Software ist kein Produkt das wir verkaufen. Wir helfen unseren Kunden, indem wir sie beraten. Die Wertschöpfung die wir dabei leisten, stellen wir in Form von eingesetzter Zeit in Rechnung.

(pause: 1)

Ich denke damit wird klar von welcher essentiellen Bedeutung das Erfassen von geleisteten Stunden für uns ist. Dies ist unsere Einnahmequelle für unsere Gehälter, unsere Ausstattung, unsere Investitionen in die Zukunft - einfach alles um ökonomisch zu überleben und zu wachsen.

(pause: 1)

Deswegen möchte ich an dieser Stelle sehr klar werden:

(pause: 1)

das zuverlässige Buchen von Stunden liegt bei jedem und jeder, der oder die Arbeit bei unseren Kunden leistet, in der persönlichen Verantwortung.

(pause: 1)

Dieses Video soll euch die zu beachtenden Dinge und das Verwenden unseres internen Tools, L I time nahebringen.

(pause: 2)
---
![contain](slides/li-time/Folie3.png)

Lasst uns anschauen was unsere Standards beim Erfassen von Zeiten sind.

(pause: 1)

Wir erfassen unsere Stunden regelmäßig. Das bedeutet mindestens wöchentlich. Nur zum Monatsende rückblickend die Zeiten zu erfassen führt häufig zu Mehraufwand, um die eigens geleistete Arbeit nachvollziehen zu können. Außerdem ist das unter-monatliche Melden von ökonomischen Kennzahlen an den Aufsichtsrat obligatorisch. Hierfür sind möglichst aktuelle Zahlen wichtig.

(pause: 1)

Außerdem reichen wir unsere Stunden pünktlich ein. Am letzen Arbeitstag eines Monats werden die Stunden offiziell eingereicht, um den Rechnungslauf zu ermöglichen. Bitte beachtet dass der letzte Arbeitstag in Abhängigkeit von Arbeitszeiten oder Urlaub individuell ist. Du kennst Deinen letzten Arbeitstag eines Monats und weißt selbst am Besten wann die Zeiten eingereicht werden müssen. Dass Stunden pünktlich, zuverlässig und ohne Aufforderung erfasst werden, muss eine Selbstverständlichkeit sein.

(pause: 2)

Wichtig ist die Vollständigkeit der geleisteten Stunden. Dabei ist wichtig zu wissen was zur geleisteten Wertschöpfung dazu gehört. So ist beispielsweise nicht nur das Schreiben von Code geleistete Arbeit, sondern auch Dinge wie das Einarbeiten in Themen, die Teilnahme an Terminen, das Vorbereiten von Terminen und weitere Dinge die notwendig sind um Dich arbeitsfähig zu machen.

(pause: 2)

Alle unsere Consultants gehen verantwortlich mit dem Buchen von Stunden um. Das bedeutet auch, dass wir uns mit Beauftragungen auseinandersetzen. Typische Fragen für Dich sind: welche Standards halten wir im Projekt beim Erfassen von Stunden ein? Auf welches Budget buche ich? Ändert sich etwas an meiner Beauftragung und ist mein Geschäftskreis darüber informiert?

(pause: 2)

Wir sind uns bewusst, dass das Abrechnen von Stunden mit Vertrauen einhergeht. Unsere Kunden haben das gute Recht nachvollziehen zu können was wir geleistet haben. Somit ist es wichtig dass unsere Beschreibung der Arbeitszeit ausreichend informativ ist, um unsere Wertschöpfung transparent zu machen. Die in L I Time eingereichten Kommentare gehen mit der Rechnung auch an den Kunden. Daraus ergibt sich automatisch die hohe Qualität die wir an die Informationen stellen.


(pause: 2)
---
![contain](slides/li-time/Folie4.png)

Lasst uns nun aber zum konkreten doing kommen: dem Eintragen von Leistungen. L I time ist das Tool unserer Wahl, das bei esenntrie selbst entwickelt wurde. Für jeden erreichbar ist dieses Tool mit Okta. Damit bist Du direkt autorisiert und hast Einblick in die Dir zugeteilten Budgets sowie Deiner eingetragenen Leistungen.


(pause: 1)
---
![contain](slides/li-time/Folie5.png)

Einstiegspunkt ist das Dashboard, das einen Überblick über die aktuellen Projekte liefert.


---
![contain](slides/li-time/Folie6.png)

Von hier kannst Du in die Zeiterfassung springen.

(pause: 1)
---
![contain](slides/li-time/Folie7.png)

Um Zeiten für ein Projekt erfassen zu können, muss eine Zeile zum aktuellen Monat hinzugefügt werden. Du wählst den Kunden, das Projekt und die Leistungsart. Bei mehreren Leistungsarten ist mit der Projektleitung abzusprechen auf welches Budget Du buchen kannst. Dies kann von Tätigkeit und Erfahrungslevel abhängig sein.

(pause: 1)

Wie im Bild dargestellt, können auch Zeiten auf intern gebucht werden. Das ist hilfreich um Deine Arbeitszeit zu tracken, wenn Du auf keinem Projekt bist. Oder um zu sehen, wie viel Du aktuell für interne und wie viel für externe Tätigkeiten investierst.


(pause: 2)
---
![contain](slides/li-time/Folie8.png)

Ist eine Projekt-Zeile hinzugefügt, können tageweise die Zeiten erfasst werden.

(pause: 1)
---
![contain](slides/li-time/Folie9.png)

Mit Klick in die Zelle eines Tages lässt sich die geleistete Gesamtsumme der Stunden dieses Tages eintragen. Es können im unteren Bereich auch der Start- und Endzeitpunkt sowie die Pausenzeit eingetragen werden. Die Summe des Tages wird dann automatisch berechnet.

(pause: 1)

Wichtig ist der Kommentar zur selektierten Zeile. Hier wird die geleistete Arbeit ausreichend informativ beschrieben. Für Developer ist es üblich Ticket-Nummern oder ähnliches mit einer kurzen Beschreibung zu nennen. Auch Termine können Teil der Beschreibung sein. Die Rechtschreibung ist zu beachten.

(pause: 2)
---
![contain](slides/li-time/Folie10.png)

Mit Hilfe des Drucker-Symbols am rechten Rand lässt sich anzeigen wie sich die erfassten Zeiten darstellen. Dieser Leistungsschein wird im Rechnungslauf dem Kunden zugestellt.

(pause: 2)
---
![contain](slides/li-time/Folie11.png)

Mit Hilfe des Pfeil-Symbols wird der Leistungsschein zum Monatsende eingereicht. Dies ist an Deinem letzten Arbeitstag eines Monats zu tun. Zuvor den Leistungsschein bitte sorgfältig prüfen, um das Einreichen von fehlerhaften Leistungen mit anschließend verbundenen Korrekturprozessen zu vermeiden.

(pause: 2)
---
![contain](slides/li-time/Folie12.png)

Hier die Darstellung eines möglichen Leistungsscheins, aufgerufen mit dem Drucker-Symbol.

(pause: 3)
---
![contain](slides/li-time/Folie13.png)

Und hier der Blick auf eingereichte Projektzeilen zum Monatsende.

(pause: 3)
---
![contain](slides/li-time/Folie1.png)

Ich hoffe ich konnte Dir beim Erfassen Deiner Stunden mit L I time helfen. Wende Dich bei Fragen gerne an die Ökonomen-Rolle oder die Projektleitung. Bis bald!

(pause: 2)
---
