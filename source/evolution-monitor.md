---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/evolution-monitor/Folie1.png)

(pause: 1)

Hallo und herzlich willkommen zum *[evolution]{en}* Monitor Lernvideo.

(pause: 2)
Mithilfe des *[evolution]{en}* Monitors kannst du deine Ideen zur Entscheidungsreife entwickeln, sorgst für Transparenz im Unternehmen und kannst bei der praktischen Umsetzung von Vorschlägen wertvolle Erfahrungen sammeln.

(pause: 1)

In diesem Video möchten wir dir erklären, weshalb es den *[evolution]{en}* Monitor bei esenntrie gibt und wie Du ihn nutzen kannst.

(pause: 1)

Bei weiterführenden Fragen kannst du gerne auf das *[O and G]{en}* Team zukommen.

(pause: 2)
---
![contain](slides/evolution-monitor/Folie2.png)

Zu Beginn dieses Videos zeigen wir dir, was der *[evolution]{en}* Monitor ist.

(pause: 1)

Anschließend gehen wir näher auf den Prozess ein und zeigen dir die einzelnen Schritte auf dem Weg zur Einreichung eines Vorschlags. Der Prozess beginnt mit dem Ausfüllen des *[evolution]{en}* Canvas in Confluence sowie dem Erstellen des *[evolution]{en}* Tickets im Jira Board des *[evolution]{en}* Monitors.

(pause: 1)

Dieses Ticket durchläuft dann einen Prozess der vom Dienstleistungskreis *[O and G]{en}* unterstützt und anschließend vom Koordiationskreis Strategie & Vision beziehungsweise dem Vorstand bewertet wird.

(pause: 1)

Starten wir mit unserem Why.

(pause: 2)
---
![contain](slides/evolution-monitor/Folie3.png)

Mit dem *[evolution]{en}* Monitor haben wir einen Prozess geschaffen, der die selbstorganisierte und selbstverantwortliche Unternehmensentwicklung durch alle Mitarbeitenden vorantreibt.

(pause: 1)

Wir glauben daran, dass Vorsprung nur erzeugt werden kann wenn jede und jeder zufrieden und motiviert ist, sowie eigenständig und selbstverantwortlich Handeln kann.

(pause: 1)

Der *[evolution]{en}* Monitor unterstützt das, indem jeder bei esenntrie kreisübergreifende Vorschläge einbringen und schlussendlich auch zur praktischen Umsetzung beitragen kann.

(pause: 1)

Damit wird unter anderem die Selbstorganisation jedes Einzelnen und jeder Einzelnen ermöglicht, die Zukunftsfähigkeit des Unternehmens gesichert sowie Transparenz im Unternehmen geschaffen.


(pause: 2)
---
![contain](slides/evolution-monitor/Folie4.png)

Lass uns einen näheren Blick auf den *[evolution]{en}* Monitor und die damit einhergehenden Begriffe werfen.

(pause: 1)

Der *[evolution]{en}* Prozess beschreibt, den gesamten Prozess von der Einreichung eines Vorschlags über dessen Ausarbeitung, die Bewertung bis hin zur Umsetzung.

(pause: 1)

Bei einem Vorschlag sollte es sich um eine größere organisatorische Veränderung handeln, die eine kreisübergreifende Wirkung auf Kollegen, Prozesse oder IT hat.

(pause: 2)

Dabei kann es sich beispielsweise um die Konstitution eines Kreises oder strategisch relevante Initiativen handeln.

(pause: 1)

Ein *[evolution]{en}* Ticket im *[evolution]{en}* Monitor stellt einen solchen Vorschlag dar, dessen Status anhand des Boards eingesehen werden kann.

(pause: 1)

Wir möchten jeden esenntrie dazu ermutigen, Vorschläge im Rahmen des E M einzubringen. Wer einen Vorschlag einbringt, sollte sich allerdings auch für die Ausarbeitung und Umsetzung verantwortlich fühlen - natürlich bei Bedarf mit Unterstützung von interessierten Kollegen und Kolleginnen.

(pause: 2)
---
![contain](slides/evolution-monitor/Folie5.png)

Nun zum *[evolution]{en}* Prozess.

(pause: 1)

Du hast einen Vorschlag, der wie bereits erwähnt eine kreisübergreifende Wirkung hat, beziehungsweise die gesamte Organisation betrifft.

(pause: 1)

Deiner Kreativität sind hier keine Grenzen gesetzt - bedenke nur, dass du auch für die Umsetzung zuständig bist.

(pause: 1)

Diesen Vorschlag musst du nun in den *[evolution]{en}* Monitor einbringen.

(pause: 1)

Einerseits bedeutet das die Erstellung eines *[evolution]{en}* Tickets im *[evolution]{en}* Monitor Kanban Board in der Spalte “Vorschlag”. Andererseits muss dein Vorschlag durch Ausfüllen des *[evolution]{en}* Canvas in Confluence im Bereich "ORG *[evolution]{en}*" konkretisiert werden.

(pause: 1)

Diese beiden Schritte gehen Hand in Hand. Mit welchem du dabei beginnst ist dir überlassen.

(pause: 1)

Nachdem du deinen Vorschlag mithilfe des *[evolution]{en}* Canvas vollständig ausgearbeitet hast und mit deinem Ticket verknüpft hast, kannst du dein Ticket in die Spalte “Überprüfung” verschieben.

(pause: 1)

Das *[O and G]{en}* Team wird automatisch darüber informiert und überprüft deinen Vorschlag nun auf formale Kriterien.

(pause: 1)

Sobald dieser Schritt geschafft ist, wird dein Ticket durch *[O and G]{en}* in die Spalte “Bewertung” geschoben.

(pause: 1)

Es liegt nun am Kreis Strategie & Vison sowie dem Vorstand darüber zu entscheiden, ob dein Vorschlag in die Umsetzung geht oder nicht.

(pause: 1)

Abhängig von der Entscheidung wird dein Vorschlag anschließend entweder umgesetzt oder nicht. Wenn der Aufwand der Umsetzung verfolgt werden soll, ist ein Engagement in Kimble anzulegen. Außerdem darfst Du deinen Vorschlag im kommenden Vor 12 vorstellen. Dies sorgt für eine Sichtbarkeit im gesamten Unternehmen.

(pause: 2)
---
![contain](slides/evolution-monitor/Folie6.png)


Lass uns einen näheren Blick in die Einreichung eines Vorschlags werfen.

(pause: 1)

Als Entscheidungsgrundlage gilt das sogenannte *[evolution]{en}* Canvas. Für jeden eingereichten Vorschlag muss dieses Canvas ausgefüllt werden und dient der Konkretisierung einer Idee. Du findest das Canvas in Confluence im Bereich ORG *[evolution]{en}*. Auf der Seite “02 - Vorschläge” kannst du auf den Button “Hier einen Vorschlag erstellen” klicken, woraufhin eine neue Unterseite mit einem leeren Canvas erstellt wird.

(pause: 1)

Ziel des Canvas ist es, deinen Vorschlag aus verschiedenen Perspektiven zu beleuchten. Dazu solltest du deine Hauptidee beschreiben und zudem aufzeigen, welches Problem damit gelöst wird und welcher Nutzen erzeugt wird. Für eine kritische Betrachtung des Vorschlags dienen die Felder Alternative Ideen und Risiken. Außerdem wird im Canvas beschrieben, wer an der Idee mitarbeiten würde, wen es betrifft und und ein Sponsor aus dem Vorstand ist zu benennen. 

(pause: 1)

Weiterhin sollen Erfolgskriterien deiner Idee aufgezeigt und nächste Schritte definiert werden. Auch eine Schätzung des benötigten Budgets ist abzugeben, wie lange die Initiative voraussichtlich dauern wird und wann damit begonnen werden soll. Mit der Erstellung des Canvas geht die Erstellung des Tickets im *[evolution]{en}* Monitor einher, dies wird nun näher erläutert.

(pause: 2)
---
![contain](slides/evolution-monitor/Folie7.png)

Um ein neues Ticket einzureichen, rufst du das *[evolution]{en}* Monitor Kanban Board in Tschira auf und klickst im Reiter Vorschlag den Button “Vorgang erstellen”. In der darauf erscheinenden Maske kannst du deinem Ticket einen Namen geben, die Idee kurz beschreiben sowie dich oder eine andere Person als Verantwortliche nennen. Des Weiteren solltest du deinen Vorgang mit der bereits entstandenen Seite “*[evolution]{en}* Canvas” verknüpfen. Weitere Felder müssen nicht ausgefüllt werden.

(pause: 1)

Nach der Erstellung erscheint das Ticket automatisch in der Spalte “Vorschlag”. Hier bleibt es so lange stehen, bis du im *[evolution]{en}* Canvas deinen Vorschlag ausreichend konkretisiert hast. Sobald du das Gefühl hast, das erreicht zu haben, kannst du dein Ticket in die Spalte “Überprüfung” verschieben. Das *[O and G]{en}* Team erhält automatisch eine Benachrichtigung darüber und prüft deinen Vorschlag nun auf formale Kriterien.

(pause: 2)
---
![contain](slides/evolution-monitor/Folie8.png)

Nach erfolgreicher Überprüfung kann dein Vorschlag bewertet werden. Hierzu werden beispielsweise Kriterien wie kreisübergreifende Relevanz, Einklang des Vorschlags mit der Unternehmensstrategie und das Verhältnis von Kosten zum erwarteten Nutzen betrachtet.

(pause: 1)

Je nach Beschluss wird dein Ticket im Anschluss daran in die Spalte “Umsetzen” oder “Nicht umgesetzt” geschoben. Zudem erhälst du eine kurze Notiz in deinem Ticket mit der Entscheidung. Wird dein Vorschlag umgesetzt, so bist du nun für die Umsetzung verantwortlich und kannst dein Team zusammenstellen.

(pause: 1)

Bei einer Nicht-Umsetzung ist keine weitere Aktion nötig, dein Canvas bleibt aber weiterhin gespeichert und kann gegebenenfalls zu einem späteren Zeitpunkt wieder eingebracht oder ggf. nachbearbeitet werden.

(pause: 2)
---
![contain](slides/evolution-monitor/Folie9.png)

Alle laufenden und vergangenen Vorgänge können in Tschira eingesehen werden. Im ORG *[evolution]{en}* Bereich sind die Canvas in ihrer jeweiligen Phase im Prozess auffindbar. Um die Transparenz zu fördern und alle auf dem Laufenden zu halten, sind die Felder Status und Fortschritt aktuell zu halten. Außerdem können in den Kommentaren und über die Vorstellung von Zwischenergebnissen der Fortschritt transparent gemacht werden.


(pause: 2)
---
![contain](slides/evolution-monitor/Folie10.png)

Wir freuen uns,  wenn dir dieses Video weitergeholfen hat und du dich nun bereit fühlst, deinen Vorschlag in den *[evolution]{en}* Monitor einzubringen.

(pause: 1)

Bitte beachte, dass es sich beim *[evolution]{en}* Prozess um einen sich ständig weiterentwickelnden Prozess handelt, weshalb sich einzelne Schritte oder Details unter Umständen ändern können. Wir versuchen, dieses Video immer aktuell zu halten und anzupassen.

Zögere bitte nicht, dich bei weiteren Fragen an das *[O and G]{en}* Team zu wenden. Vielen Dank!  

(pause: 2)
---
