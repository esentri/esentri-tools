---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/how-to-ilse/Folie1.png)

(pause: 3)

Hallo und herzlich willkommen zum Kurs How to - Ilse.

(pause: 2)
---
![contain](slides/how-to-ilse/Folie2.png)


Mein Name ist Ilse und ich gebe Dir heute eine kurze Einführung dazu, wie wir gemeinsam Videos erstellen können. Kurz gesagt: Du stellst mir alles zur Verfügung - Text Bilder und Musik - und ich bastel daraus ein Video für Dich. Aber Schritt für Schritt.

(pause: 2)
---
![contain](slides/how-to-ilse/Folie3.png)

Was wir heute machen werden:

(pause: 1)

Wir gehen durch die wichtigsten Dinge, die wir für ein strukturiertes Arbeiten brauchen. Zum einen, wie wir bei esenntrie mit Hilfe von git verteiltes Arbeiten an mir und meinen Videos ermöglichen. Wir sprechen über narakiit, das Produkt hinter Ilse. Wir einigen uns auf eine einheitliche Struktur in unseren Projekten. Wir sprechen darüber, wie ich deine erstellten Dateien wie Bilder und Worte zum Leben erwecke. Und abschließend tauschen wir noch ein paar Tipps und Tricks aus.

(pause: 2)
---
![contain](slides/how-to-ilse/Folie4.png)

Sprechen wir kurz über Bitbucket. Wenn Du eine Idee für einen Academy Inhalt hast, dann startest Du am besten von Anfang an mit einem Repository. So kannst Du in anderen repositories Dinge abschauen. Außerdem hast Du direkt eine Kontrolle über die Versionierung. Im KG Academy Repository sind alle Videos abgelegt.

(pause: 2)
---
![contain](slides/how-to-ilse/Folie5.png)

Du brauchst mehrere Dinge, um via narakiit ein Video erstellen zu können. Du brauchst Node und NPM. Außerdem musst du den Client für die Kommandozeile installieren. Auf der Homepage von narakiit findest Du Hilfe.

(pause: 2)
---
![contain](slides/how-to-ilse/Folie6.png)

Kommen wir zur einheitlichen Struktur. Jedes Projekt hat die folgenden Ordner - Folie, generated und source.

(pause: 1)

Der Folien Ordner ist der Platz für das Bearbeiten der Folien. In der Regel findest Du hier eine Power Point Präsentation.

(pause: 1)

Im Ordner generated werden die erstellten Videos abgelegt. Das ist auch im generate Shell Skript hinterlegt, das im Haupt Ordner liegt.

(pause: 1)

Unter Source stellst Du mir alles zur Verfügung was ich für die Erstellung brauche: Text als Markdown, Folien als PNG exportiert aus der Präsentation, und eventuell auch Audio Dateien, die im Hintergrund abgespielt werden sollen. Wie dieses Lied hier.

(pause: 4)

Wenn wir diese einheitliche Struktur einhalten, haben wir nicht nur den Vorteil dass man das Skript zur Generierung nur wenig anpassen muss. Sondern man findet sich auch in anderen Projekten schnell zurecht und kann diese anpassen.   

(pause: 2)
---
![contain](slides/how-to-ilse/Folie7.png)

Aber wie bringst Du mich jetzt zum Sprechen.

(pause: 1)

Zentral sind die Markdown Dateien. Diesen kannst Du wie einen Bauplan verstehen. Über das Markdown können ein paar Einstellungen wie Übergänge der Hintergrund, oder die Stimme getroffen werden. Insbesondere liegt hier aber der Text den ich während den Szenen vorlese. Eine Szene besteht in der Regel aus einer Folie die im Markdown mit einem Befehl aufgerufen wird. Eine Szene wird von der nächsten immer mit drei Strichen abgetrennt. Du kannst auch Pausen definieren, sodass ich etwas pointierter lesen kann.

(pause: 1)

Nutze für das Bearbeiten von Markdown Dateien ein geeignetes Tool, das dir beispielsweise auch Rechtschreibfehler anzeigt. Achte auch auf die Verwendung von Satzzeichen. Sie lassen mich - richtig eingesetzt - im Flow bleiben.

(pause: 2)
---
![contain](slides/how-to-ilse/Folie8.png)

Endlich! Wir erstellen das Video!

(pause: 1)

Wenn Du das im Haupt Ordner hinterlegte Skript aufrufst verwende ich die narakeet API und lasse das Video rendern. Abschließend stelle ich es Dir im bekannten generated Ordner zur Verfügung.

(pause: 1)

Beim Generieren des Videos fallen dann auch fehlende Dateien oder falsche Befehle im Markdown auf. Bitte denke auch an das aktuell Halten des repositories.

(pause: 2)
---
![contain](slides/how-to-ilse/Folie9.png)

Kommen wir abschließen zu den Tipps und Tricks. Manche Wörter spreche ich etwas komisch aus. Zusammen können wir das ändern.

(pause: 1)

Zum Beispiel: Wenn Du in Dein Markdown schreibst, dass ich esentri sagen soll, dann sage ich esentri. Mir fällt es leichter esenntrie zu sagen. Ich weiß: nicht perfekt aber ein Anfang.

Ähnlich bei easy develop. Wenn Du easy develop schreibst, klingt es komisch. Hier klingt es besser, wenn Du mich easy deewellopp sagen lässt.

(pause: 2)

Ich danke Dir und bin gespannt welche kleine Hürden Du kennenlernst und wie wir sie hier den anderen zugänglich machen können.

(pause: 1)
---
![contain](slides/how-to-ilse/Folie1.png)

Jetzt lass uns aber loslegen mit Deinem ersten Video. Bis bald!
---
